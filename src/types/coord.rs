use super::{Altitude, Degree};

/// Geodesic coordinate type
/// note: altitude is calculated in meters
#[derive(Debug, Clone, PartialEq)]
pub struct Coord {
    pub lat: Degree,
    pub lon: Degree,
    pub alt: Altitude,
}

impl Eq for Coord {}

impl Coord {
    pub fn new(lat: Degree, lon: Degree, alt: Altitude) -> Self {
        Self { lat, lon, alt }
    }
}
