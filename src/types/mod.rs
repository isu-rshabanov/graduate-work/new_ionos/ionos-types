pub mod coord;

pub type Degree = f64;
pub type Altitude = f64;
