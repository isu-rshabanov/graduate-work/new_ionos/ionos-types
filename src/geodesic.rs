use geographiclib_rs::{DirectGeodesic, Geodesic, InverseGeodesic};

use crate::types::{coord::Coord, Altitude, Degree};

#[derive(Debug, Clone, PartialEq, PartialOrd)]
pub struct GeodesicAdapter {
    geodesic: Geodesic,
}

impl GeodesicAdapter {
    pub fn wgs84() -> Self {
        Self {
            geodesic: Geodesic::wgs84(),
        }
    }

    pub fn new(geodesic: Geodesic) -> Self {
        Self { geodesic }
    }

    /// Creates destination point
    /// alt - altitude (in meters)
    pub fn direct(&self, source: &Coord, azimuth: Degree, alt: Altitude) -> Coord {
        let (lat, lon) = self.geodesic.direct(source.lat, source.lon, azimuth, alt);
        Coord { lat, lon, alt }
    }

    pub fn distance(&self, lhs: &Coord, rhs: &Coord) -> f64 {
        self.geodesic.inverse(lhs.lat, lhs.lon, rhs.lat, rhs.lon)
    }
}
